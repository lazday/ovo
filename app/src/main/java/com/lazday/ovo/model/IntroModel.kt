package com.lazday.ovo.model

data class IntroModel(
    val id: Int,
    val title: String,
    val image: Int,
)
