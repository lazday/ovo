package com.lazday.ovo.model

data class PurchaseModel(
    val title: String,
    val image: Int,
)
