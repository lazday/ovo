package com.lazday.ovo.model

data class RecommendationModel(
    val title: String,
    val subtitle: String,
    val image: Int,
)
