package com.lazday.ovo.model

data class InterestModel(
    val id: Int,
    val title: String,
    val subtitle: String,
    val linkTitle: String,
    val linkUrl: String,
    val image: Int,
)
