package com.lazday.ovo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import com.lazday.ovo.adapter.*
import com.lazday.ovo.databinding.ActivityHomeBinding
import com.lazday.ovo.model.*

class HomeActivity : AppCompatActivity() {

    private val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        binding.toolbar.inflateMenu(R.menu.menu_notification)
        val menu = binding.toolbar.menu
        menu.findItem(R.id.action_notification).setOnMenuItemClickListener {
            Toast.makeText(applicationContext, "Notifications", Toast.LENGTH_SHORT).show()
            true
        }

        binding.textCash.text = "238.500"
        binding.textPoint.text = "8740"

        binding.imageTopup.setOnClickListener {
            Toast.makeText(applicationContext, "Top Up", Toast.LENGTH_SHORT).show()
        }

        binding.listPurchase.adapter = purchaseAdapter

        binding.viewPromo.adapter = promoAdapter
        binding.indicatorPromo.setViewPager2( binding.viewPromo )

        binding.listRecommendation.adapter = recommendationAdapter

        binding.listIntro.adapter = introAdapter

        binding.buttonFinancial.setOnClickListener {
            Toast.makeText(applicationContext, "Start Financial", Toast.LENGTH_SHORT).show()
        }

        binding.listInterest.adapter = interestAdapter
    }

    private val purchaseAdapter by lazy {
        val items = listOf<PurchaseModel>(
                PurchaseModel("PLN", R.drawable.electric),
                PurchaseModel("Pulsa", R.drawable.ic_phone),
                PurchaseModel("Vouche Game", R.drawable.ic_game),
                PurchaseModel("Invest", R.drawable.ic_invest),
                PurchaseModel("BPJS", R.drawable.ic_bpjs),
                PurchaseModel("Internet & TV kabel", R.drawable.ic_tv),
                PurchaseModel("Proteksi", R.drawable.ic_proteksi),
                PurchaseModel("Lainnya", R.drawable.ic_other),
        )
        PurchaseAdapter(items, object : PurchaseAdapter.AdapterListener {
            override fun onClick(purchase: PurchaseModel) {
                Toast.makeText(applicationContext, purchase.title, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private val promoAdapter by lazy {
        val items = listOf<PromoModel>(
            PromoModel(1, R.drawable.img_promo),
            PromoModel(2, R.drawable.img_promo),
            PromoModel(3, R.drawable.img_promo),
            PromoModel(4, R.drawable.img_promo),
        )
        PromoAdapter(items, object : PromoAdapter.AdapterListener {
            override fun onClick(promo: PromoModel) {
                Toast.makeText(applicationContext, promo.id.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private val recommendationAdapter by lazy {
        val items = listOf<RecommendationModel>(
            RecommendationModel("Proteksi - Elektronik", "Cashback 25% Proteksi HP", R.drawable.ic_phone),
            RecommendationModel("Proteksi - Sepeda", "Cashback 25% Proteksi Sepeda", R.drawable.ic_phone),
            RecommendationModel("Proteksi - Elektronik", "Reksa Dana Pasar Uang Manulife OVO", R.drawable.ic_phone),
        )
        RecommendationAdapter(items, object : RecommendationAdapter.AdapterListener {
            override fun onClick(recommendation: RecommendationModel) {
                Toast.makeText(applicationContext, recommendation.title, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private val introAdapter by lazy {
        val items = listOf<IntroModel>(
            IntroModel(1, "KEUANTUNGAN PAKAI OVO", R.drawable.img_introduction),
            IntroModel(2, "CARA TOPUP OVO CASH", R.drawable.img_introduction),
            IntroModel(3, "UPGRADE KE OVO PREMIUM", R.drawable.img_introduction),
        )
        IntroAdapter(items, object : IntroAdapter.AdapterListener {
            override fun onClick(intro: IntroModel) {
                Toast.makeText(applicationContext, intro.id.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }

    private val interestAdapter by lazy {
        val items = listOf<InterestModel>(
            InterestModel(
                1,
                "KEUANTUNGAN PAKAI OVO",
                "Punya kendala atau pertanyaan terkait OVO? kamu bisa kirim disini",
                "Lihat Bantuan",
                "https://lazday.com",
                R.drawable.img_interest
            ),
            InterestModel(
                2,
                "EDUKASI INVESTASI",
                "Hemat uang ngopi cantik untuk investasi",
                "Cari tahu disini!",
                "https://lazday.com",
                R.drawable.img_interest
            ),
        )
        InterestAdapter(items, object : InterestAdapter.AdapterListener {
            override fun onClick(interest: InterestModel) {
                Toast.makeText(applicationContext, interest.linkUrl, Toast.LENGTH_SHORT).show()
            }
        })
    }
}