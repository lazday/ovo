package com.lazday.ovo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.ovo.databinding.AdapterPurchaseBinding
import com.lazday.ovo.model.PurchaseModel

class PurchaseAdapter (
        var items: List<PurchaseModel>,
        var listener: AdapterListener?
): RecyclerView.Adapter<PurchaseAdapter.ViewHolder>() {

    class ViewHolder(val binding: AdapterPurchaseBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterPurchaseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.title.text = item.title
        holder.binding.image.setImageResource(item.image)
        holder.itemView.setOnClickListener {
            listener?.onClick( item )
        }
    }

    interface AdapterListener {
        fun onClick(purchase: PurchaseModel)
    }

}