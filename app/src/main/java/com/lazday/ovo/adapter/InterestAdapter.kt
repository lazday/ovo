package com.lazday.ovo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.ovo.databinding.AdapterInterestBinding
import com.lazday.ovo.model.InterestModel

class InterestAdapter (
        var items: List<InterestModel>,
        var listener: AdapterListener?
): RecyclerView.Adapter<InterestAdapter.ViewHolder>() {

    class ViewHolder(val binding: AdapterInterestBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterInterestBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.textTitle.text = item.title
        holder.binding.textSubtitle.text = item.subtitle
        holder.binding.textLink.text = item.linkTitle
        holder.binding.textLink.setOnClickListener {
            listener?.onClick( item )
        }
    }

    interface AdapterListener {
        fun onClick(intro: InterestModel)
    }

}