package com.lazday.ovo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.ovo.databinding.AdapterIntroBinding
import com.lazday.ovo.databinding.AdapterPurchaseBinding
import com.lazday.ovo.model.IntroModel
import com.lazday.ovo.model.PurchaseModel

class IntroAdapter (
        var items: List<IntroModel>,
        var listener: AdapterListener?
): RecyclerView.Adapter<IntroAdapter.ViewHolder>() {

    class ViewHolder(val binding: AdapterIntroBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterIntroBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.textTitle.text = item.title
        holder.itemView.setOnClickListener {
            listener?.onClick( item )
        }
    }

    interface AdapterListener {
        fun onClick(intro: IntroModel)
    }

}