package com.lazday.ovo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.ovo.databinding.AdapterRecommendationBinding
import com.lazday.ovo.model.RecommendationModel

class RecommendationAdapter (
    var items: List<RecommendationModel>,
    var listener: AdapterListener?
): RecyclerView.Adapter<RecommendationAdapter.ViewHolder>() {

    class ViewHolder(val binding: AdapterRecommendationBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        AdapterRecommendationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.title.text = item.title
        holder.binding.subtitle.text = item.subtitle
        holder.itemView.setOnClickListener {
            listener?.onClick( item )
        }
    }

    interface AdapterListener {
        fun onClick(purchase: RecommendationModel)
    }

}