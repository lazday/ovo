package com.lazday.ovo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lazday.ovo.databinding.AdapterPromoBinding
import com.lazday.ovo.model.PromoModel

class PromoAdapter (
        var items: List<PromoModel>,
        var listener: AdapterListener?
): RecyclerView.Adapter<PromoAdapter.ViewHolder>() {

    class ViewHolder(val binding: AdapterPromoBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder (
        AdapterPromoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.image.setImageResource(item.image)
        holder.itemView.setOnClickListener {
            listener?.onClick( item )
        }
    }

    interface AdapterListener {
        fun onClick(purchase: PromoModel)
    }

}